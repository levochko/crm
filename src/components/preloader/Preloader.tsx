import { CircularProgress } from '@material-ui/core';
import { FunctionComponent, ComponentProps } from 'react';
import React from 'react';

export interface PreloaderProps extends ComponentProps<'div'> {
  color?: 'primary' | 'secondary' | 'inherit';
  size?: number;
}

export const Preloader: FunctionComponent<PreloaderProps> = props => {
  const { color, size = 15, ...rest } = props;
  return (
    <div {...rest}>
      <CircularProgress color={color} size={size} />
    </div>
  );
};
