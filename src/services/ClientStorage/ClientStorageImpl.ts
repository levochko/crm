import { ClientStorage } from './ClientStorage';
import { isDefined } from 'utils';
import { injectable } from 'inversify';

type SnapshotItem = { key: string; value: any } | undefined;

@injectable()
export class ClientStorageImpl extends ClientStorage {
  get<TValue>(key: string): TValue | undefined {
    const item = localStorage.getItem(key);
    if (!isDefined(item)) return;
    return JSON.parse(item) as TValue;
  }

  set<TValue>(key: string, value: TValue): void {
    localStorage.setItem(key, JSON.stringify(value));
  }

  remove(key: string): void {
    localStorage.removeItem(key);
  }

  clear(): void {
    localStorage.clear();
  }

  getSnapshot(): SnapshotItem[] {
    return Array(localStorage.length)
      .fill(null)
      .map(
        (_, index): SnapshotItem => {
          const key = localStorage.key(index);
          if (!isDefined(key)) return;
          return { key, value: localStorage.getItem(key) };
        }
      );
  }
}
