import { injectable } from 'inversify';

@injectable()
export abstract class ClientStorage {
  public abstract get<TValue>(key: string): TValue | undefined;
  public abstract set<TValue>(key: string, value: TValue): void;
  public abstract remove(key: string): void;
  public abstract clear(): void;
  public abstract getSnapshot(): any[];
}
