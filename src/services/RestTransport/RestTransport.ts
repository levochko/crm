import { injectable } from 'inversify';

@injectable()
export abstract class RestTransport {
  public abstract post<TResponse, TPayload extends object = object>(
    url: string,
    payload?: TPayload
  ): Promise<TResponse | undefined>;
  public abstract update<TResponse, TPayload extends object = object>(
    url: string,
    payload?: TPayload
  ): Promise<TResponse | undefined>;
  public abstract delete<TResponse, TPayload extends object = object>(
    url: string,
    payload?: TPayload
  ): Promise<TResponse | undefined>;
  public abstract get<TResponse>(url: string): Promise<TResponse | undefined>;

  public abstract authorize<TResponse>(
    login: string,
    password: string
  ): Promise<TResponse | undefined>;
  public abstract authorizeByToken<TResponse>(): Promise<TResponse | undefined>;
  public abstract logout(): void;
}
