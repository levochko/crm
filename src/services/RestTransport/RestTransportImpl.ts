import { RestTransport } from './RestTransport';
import { observable, computed, action } from 'mobx';
import { extractDependency } from 'di';
import { Env } from 'services/Env';
import { ClientStorage } from 'services/ClientStorage';
import { RequestURL } from 'utils';
import { injectable } from 'inversify';

type AuthorizeResponse = {
  accessToken: string;
  refreshToken: string;
};

enum ResponseStatus {
  OK = 200
}

type Tokens = { accessToken: string | undefined; refreshToken: string | undefined };

@injectable()
export class RestTransportImpl extends RestTransport {
  private env: Env = extractDependency(Env);
  private clientStorage: ClientStorage = extractDependency(ClientStorage);

  private REFRESH_TOKEN_STORAGE_KEY = 'refreshToken';

  private rootURL = this.env.PUBLIC_URL;

  @observable
  private _accessToken: string | undefined;

  @observable
  private _refreshToken: string | undefined;

  @computed
  private get authorizationHeader(): string {
    if (!this._accessToken) return '';
    return `Bearer ${this._accessToken}`;
  }

  @action
  private setTokens(access: string | undefined, refresh: string | undefined): void {
    this._accessToken = access;
    this._refreshToken = refresh;
    if (refresh) this.clientStorage.set(this.REFRESH_TOKEN_STORAGE_KEY, refresh);
    else this.clientStorage.remove(this.REFRESH_TOKEN_STORAGE_KEY);
  }

  @computed
  private get requestHeader() {
    return {
      'Content-Type': 'application/json',
      // 'Content-Type': 'application/x-www-form-urlencoded',
      Authorization: this.authorizationHeader
    };
  }

  private prepareURL(url: string): string {
    return this.rootURL + url;
  }

  private async getUpdatedTokens(refreshToken = this._refreshToken): Promise<Tokens> {
    try {
      const rawResponse = await fetch(this.prepareURL(RequestURL.UPDATE_TOKENS), {
        method: 'GET', // POST
        headers: {
          ...this.requestHeader,
          Authorization: `Bearer ${refreshToken}`
        }
      });
      if (rawResponse.status === ResponseStatus.OK) {
        return rawResponse.json();
      }
      throw new Error();
    } catch (err) {
      return { accessToken: undefined, refreshToken: undefined };
    }
  }

  private async request<TResponse, TPayload extends object = object>(
    url: string,
    method: RequestInit['method'],
    payload?: TPayload
  ): Promise<TResponse | undefined> {
    const req = () =>
      fetch(this.prepareURL(url), {
        method,
        headers: this.requestHeader
        // body: JSON.stringify(payload)
      }).then(res =>
        res.status === ResponseStatus.OK ? (res.json() as Promise<TResponse>) : undefined
      );
    try {
      const response = await req();
      if (response === undefined) throw new Error();
      return response;
    } catch (err) {
      const tokens = await this.getUpdatedTokens();
      this.setTokens(tokens.accessToken, tokens.refreshToken);
      if (this._accessToken) {
        return;
      }
    }
  }

  private _authorize<AuthResponseData>(
    url: string,
    body: string
  ): Promise<AuthResponseData | undefined> {
    return fetch(this.prepareURL(url), {
      method: 'GET'
      // body
    })
      .then(res => res.json() as Promise<AuthResponseData & AuthorizeResponse>)
      .then(res => {
        this.setTokens(res.accessToken, res.refreshToken);
        delete res.refreshToken;
        delete res.accessToken;
        return res;
      })
      .catch(err => {
        return undefined;
      });
  }

  public authorize<AuthResponseData>(
    login: string,
    password: string
  ): Promise<AuthResponseData | undefined> {
    return this._authorize(RequestURL.AUTHORIZE, JSON.stringify({ login, password }));
  }

  public async authorizeByToken<AuthResponseData>(): Promise<AuthResponseData | undefined> {
    const refreshToken = this.clientStorage.get<string>(this.REFRESH_TOKEN_STORAGE_KEY);
    if (!refreshToken) return undefined;
    return this._authorize(RequestURL.AUTHORIZE_BY_TOKEN, JSON.stringify({ refreshToken }));
  }

  @action
  public logout(): void {
    this.setTokens(undefined, undefined);
  }

  public post<TResponse, TPayload extends object = object>(
    url: string,
    payload?: TPayload
  ): Promise<TResponse | undefined> {
    return this.request<TResponse, TPayload>(url, 'GET', payload); // POST
  }

  public update<TResponse, TPayload extends object = object>(
    url: string,
    payload?: TPayload
  ): Promise<TResponse | undefined> {
    return this.request<TResponse, TPayload>(url, 'GET', payload); // UPDATE
  }

  public delete<TResponse, TPayload extends object = object>(
    url: string,
    payload?: TPayload
  ): Promise<TResponse | undefined> {
    return this.request<TResponse, TPayload>(url, 'GET', payload); // DELETE
  }

  public get<TResponse>(url: string): Promise<TResponse | undefined> {
    return this.request<TResponse>(url, 'GET');
  }
}
