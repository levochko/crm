import { RestApi } from './RestApi';
import { AuthorizeResponse, EmailTemplateVariable } from './types';
import { injectable } from 'inversify';
import { extractDependency } from 'di';
import { RestTransport } from 'services/RestTransport';
import { RequestURL } from 'utils';

@injectable()
export class RestApiImpl extends RestApi {
  private restTransport: RestTransport = extractDependency(RestTransport);

  authorize(login: string, password: string): Promise<AuthorizeResponse | undefined> {
    return this.restTransport.authorize<AuthorizeResponse>(login, password);
  }

  authorizeByToken(): Promise<AuthorizeResponse | undefined> {
    return this.restTransport.authorizeByToken();
  }

  logout(): void {
    this.restTransport.logout();
  }

  getEmailTemplateVariables(): Promise<EmailTemplateVariable[] | undefined> {
    return this.restTransport.get(RequestURL.GET_EMAIL_TEMPLATE_VARIABLES);
  }
}
