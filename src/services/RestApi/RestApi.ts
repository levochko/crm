import { AuthorizeResponse, EmailTemplateVariable } from './types';
import { injectable } from 'inversify';

@injectable()
export abstract class RestApi {
  public abstract authorize(
    login: string,
    password: string
  ): Promise<AuthorizeResponse | undefined>;

  public abstract authorizeByToken(): Promise<AuthorizeResponse | undefined>;
  public abstract logout(): void;
  public abstract getEmailTemplateVariables(): Promise<EmailTemplateVariable[] | undefined>;
}
