import { UserDTO } from './UserDTO';

export type AuthorizeResponse = {
  user: UserDTO;
};
