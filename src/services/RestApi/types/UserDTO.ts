export type UserDTO = {
  name: string;
  surname: string;
  patronymic?: string;
  phone?: string;
  country?: string;
  email: string;
  avatar: string;
  role: string;
};
