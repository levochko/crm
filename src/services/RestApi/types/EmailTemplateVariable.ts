export type EmailTemplateVariable = {
  name: string;
  value: string;
};
