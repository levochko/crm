import { injectable } from 'inversify';

@injectable()
export abstract class Env {
  public readonly NODE_ENV!: 'development' | 'production' | 'test';
  public readonly PUBLIC_URL!: string;
}
