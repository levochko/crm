import { PropsWithChildren, FunctionComponent } from "react";

export type Layout<TProps extends object = object> = FunctionComponent<PropsWithChildren<TProps>>