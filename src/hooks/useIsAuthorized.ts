import { useState, useEffect } from "react"

let authorized = false

export const useIsAuthorized = (): boolean | undefined => {
    const [isAuthorized, setIsAuthorizedToState] = useState<boolean>(authorized)

    const setIsAuthorized = (value: boolean) => {
        authorized = value
        setIsAuthorizedToState(authorized)
    } 
    
    useEffect(() => {
        setTimeout(() => setIsAuthorized(true), 2000);  
    }, [isAuthorized])

    return isAuthorized
}
  