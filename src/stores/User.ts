import { observable, computed, action } from 'mobx';
import { UserDTO } from 'services/RestApi/types';

type User = UserDTO;

export class UserStore {
  private default = undefined;

  public reset(): void {
    this.setInfo(this.default);
  }

  @observable
  private _info: User | undefined = undefined;

  @computed
  get info(): User | undefined {
    return this._info;
  }

  @action
  public setInfo = (info?: User) => {
    this._info = info;
  };
}
