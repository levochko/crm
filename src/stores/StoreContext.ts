import { rootStore } from './Root';
import React from 'react';

export const StoreContext = React.createContext(rootStore);
