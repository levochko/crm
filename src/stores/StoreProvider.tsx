import React, { PropsWithChildren, FunctionComponent } from 'react';
import { Store } from './Store';
import { StoreContext } from './StoreContext';

type Props = PropsWithChildren<{
  store: Store;
}>;

export const StoreProvider: FunctionComponent<Props> = props => {
  return <StoreContext.Provider value={props.store}>{props.children}</StoreContext.Provider>;
};
