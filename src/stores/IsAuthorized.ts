import { observable, computed, action } from 'mobx';

export class IsAuthorized {
  private default = false;
  public reset(): void {
    this.set(this.default);
  }

  @observable
  private _isAuthorized = this.default;

  @computed
  get authorized(): boolean {
    return this._isAuthorized;
  }

  @action
  public set = (isAuth: boolean) => {
    this._isAuthorized = isAuth;
  };
}
