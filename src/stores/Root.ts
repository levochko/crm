import { IsAuthorized } from './IsAuthorized';
import { UserStore } from './User';
import { RestApi } from 'services';
import { extractDependency } from 'di';
import { Store } from './Store';

export class RootStore {
  private restApi = () => extractDependency(RestApi);

  isAuthorized = new IsAuthorized();
  user = new UserStore();

  logout = (): void => {
    this.isAuthorized.reset();
    this.user.reset();
    this.restApi().logout();
  };
}

export const rootStore = new RootStore();

export const getStore = (): Store => rootStore;
