import { FunctionComponent, useRef } from 'react';
import React from 'react';
import { EmailEditor } from './EmailEditor';

export const EmailConstructor: FunctionComponent = props => {
  const editor = useRef();
  return <EmailEditor ref={editor.current} />;
};
