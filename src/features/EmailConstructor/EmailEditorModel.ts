import { injectable, inject } from 'inversify';
import { observable, action, runInAction } from 'mobx';
import { rootStore, Store } from 'stores';
import { RestApi, EmailTemplateVariable } from 'services';
import { extractDependency } from 'di';

@injectable()
export class EmailEditorModel {
  @observable.shallow
  public templateVariables: EmailTemplateVariable[] | undefined;
  // fixme
  // store!: Store;
  private readonly restApi: RestApi = extractDependency(RestApi);

  public init() {
    if (this.templateVariables) return;
    // this.store = rootStore;
    this.requestTemplateVariables();
  }

  private async requestTemplateVariables() {
    const variables = await this.restApi.getEmailTemplateVariables();
    runInAction(() => {
      this.templateVariables = variables;
    });
  }
}
