import Unlayer, { EmailEditorProps } from 'react-email-editor';
import { ComponentProps, useMemo } from 'react';
import React from 'react';
import { observer } from 'mobx-react';
import { extractDependency } from 'di';
import { EmailEditorModel } from './EmailEditorModel';
import { Preloader } from 'components';
import { toJS } from 'mobx';

type Props = ComponentProps<any> & EmailEditorProps;

export const EmailEditor: React.FC<Props> = observer(props => {
  const emailEditorModel = useMemo(() => extractDependency(EmailEditorModel), []);
  emailEditorModel.init();
  const { templateVariables } = emailEditorModel;

  if (!templateVariables) {
    return <Preloader />;
  }
  return (
    <Unlayer
      options={{
        mergeTags: toJS(templateVariables)
      }}
      {...props}
    />
  );
});
