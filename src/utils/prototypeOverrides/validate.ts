import validate from "validate.js";
import { validators } from "dashboard";

export const overrideValidate = (): void => {
    validate.validators = {
        ...validate.validators,
        ...validators
      };
}
