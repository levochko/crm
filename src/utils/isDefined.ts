export const isDefined = <TValue extends unknown>(value: TValue): value is NonNullable<TValue> => {
  return value != null;
};
