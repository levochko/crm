export type MakeRequired<TInterface, TKeys extends keyof TInterface> = TInterface &
  Required<Pick<TInterface, TKeys>>
