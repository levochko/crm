export enum RequestURL {
  AUTHORIZE = '/fakes/authorize.json',
  AUTHORIZE_BY_TOKEN = '/fakes/authorize_by_token.json',
  UPDATE_TOKENS = '/fakes/update_tokens.json',
  GET_EMAIL_TEMPLATE_VARIABLES = '/fakes/get_email_template_variables.json'
}
