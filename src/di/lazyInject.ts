import getDecorators from 'inversify-inject-decorators';
import { container } from './container';
const { lazyInject: inversifyLazyInject } = getDecorators(container);

export const lazyInject = inversifyLazyInject;
