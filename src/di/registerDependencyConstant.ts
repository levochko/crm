import { interfaces } from 'inversify'

import { container } from './container'

export const registerDependencyConstant = <TDependency>(
  dependencyId: interfaces.ServiceIdentifier<TDependency>,
  dependencyValue: TDependency,
): void => {
  if (container.isBound(dependencyId)) container.unbind(dependencyId)
  container.bind<TDependency>(dependencyId).toConstantValue(dependencyValue)
}
