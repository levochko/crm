/* eslint "import/no-internal-modules": "off" */

import {
  Env,
  RestApi,
  RestApiImpl,
  RestTransport,
  RestTransportImpl,
  ClientStorage,
  ClientStorageImpl
} from 'services';

import { registerDependencyClass } from '../registerDependencyClass';
import { registerDependencyConstant } from '../registerDependencyConstant';

registerDependencyConstant<Env>(Env, process.env);
registerDependencyClass<RestApi>(RestApi, RestApiImpl);
registerDependencyClass<RestTransport>(RestTransport, RestTransportImpl);
registerDependencyClass<ClientStorage>(ClientStorage, ClientStorageImpl);
