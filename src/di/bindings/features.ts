import { EmailEditorModel } from 'features';
import { registerDependencyClass } from 'di/registerDependencyClass';

registerDependencyClass<EmailEditorModel>(EmailEditorModel);
console.log('registerDependencyClass');
