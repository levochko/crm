import { interfaces } from 'inversify'

import { container } from './container'

export const extractDependency = <TDependency>(
  dependencyId: interfaces.ServiceIdentifier<TDependency>,
): TDependency => {
  return container.get<TDependency>(dependencyId)
}
