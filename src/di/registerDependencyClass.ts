import { interfaces } from 'inversify';

import { container } from './container';

interface TDependecyImpl<TDependecy> {
  new (...args: any[]): TDependecy;
}

export const registerDependencyClass = <TDependency>(
  dependecyInterface: interfaces.ServiceIdentifier<TDependency>,
  dependecyImpl?: TDependecyImpl<TDependency>
): void => {
  if (container.isBound(dependecyInterface)) container.unbind(dependecyInterface);

  const containerWithInterface = container.bind<TDependency>(dependecyInterface);

  if (dependecyImpl) {
    containerWithInterface.to(dependecyImpl);
  } else {
    containerWithInterface.toSelf();
  }
};
