import 'reflect-metadata';
import 'di/bindings';
import React, { Component } from 'react';
import { Router } from 'react-router-dom';
import { createBrowserHistory } from 'history';
import { ThemeProvider } from '@material-ui/styles';
import 'react-perfect-scrollbar/dist/css/styles.css';
import { overrideValidate } from 'utils';
import { theme } from 'dashboard';
import { Routes } from 'routes/Routes';
import { rootStore, StoreProvider } from 'stores';
import { extractDependency } from 'di';
import { RestApi } from 'services';

const browserHistory = createBrowserHistory();

overrideValidate();

export default class App extends Component {
  private restApi = () => extractDependency(RestApi);

  render() {
    return (
      <StoreProvider store={rootStore}>
        <ThemeProvider theme={theme}>
          <Router history={browserHistory}>
            <Routes />
          </Router>
        </ThemeProvider>
      </StoreProvider>
    );
  }

  componentDidMount(): void {
    this.restApi()
      .authorizeByToken()
      .then(res => {
        if (!res) return;
        const { user } = res;
        rootStore.isAuthorized.set(true);
        rootStore.user.setInfo(user);
      });
  }
}
