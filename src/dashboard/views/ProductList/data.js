import uuid from 'uuid/v1';

export default [
  {
    id: uuid(),
    title: 'Marketing campaign',
    description: 'Contains analitycs and created marketing campaign',
    link: '/marketing',
    imageUrl: '/images/products/product_1.png',
    updatedAt: '27/03/2019'
  }
];
