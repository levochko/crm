import React from 'react';
import { Switch, Redirect } from 'react-router-dom';
import { Main as MainLayout, Minimal as MinimalLayout } from '../dashboard/layouts';

import {
  Dashboard as DashboardView,
  ProductList as ProductListView,
  UserList as UserListView,
  Typography as TypographyView,
  Icons as IconsView,
  Account as AccountView,
  Settings as SettingsView,
  SignIn as SignInView,
  NotFound as NotFoundView
} from '../dashboard/views';
import { CustomRoute } from './Route/CustomRoute';
import { MarketingCampaign } from 'features';

const Routes = () => {
  return (
    <Switch>
      <Redirect exact from="/" to="/dashboard" />
      <CustomRoute
        component={MarketingCampaign}
        exact
        layout={MainLayout}
        forAuthorized
        path="/dashboard/marketing/new"
      />
      <CustomRoute
        component={DashboardView}
        exact
        layout={MainLayout}
        forAuthorized
        path="/dashboard/marketing"
      />
      <CustomRoute
        component={ProductListView}
        exact
        layout={MainLayout}
        forAuthorized
        path="/dashboard"
      />
      <CustomRoute component={AccountView} exact layout={MainLayout} path="/account" />
      <CustomRoute component={SettingsView} exact layout={MainLayout} path="/settings" />
      <CustomRoute
        component={SignInView}
        exact
        layout={MinimalLayout}
        forAuthorized={false}
        path="/sign-in"
      />
      <CustomRoute component={NotFoundView} exact layout={MinimalLayout} path="/not-found" />
      <Redirect to="/not-found" />
    </Switch>
  );
};

export { Routes };
