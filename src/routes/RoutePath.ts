export enum RoutePath {
    BASE = '/',
    SIGN_IN = '/sign-in',
    UNKNOWN = '/500'
}
