import React, { FunctionComponent } from 'react'

export const ErrorUnknown: FunctionComponent = () => <div>Unknown error</div>