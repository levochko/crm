import React, { FunctionComponent, ReactElement } from 'react';
import { Route, RouteProps, RouteComponentProps } from 'react-router-dom';
import { Layout } from 'layouts';
import { MakeRequired } from 'utils';

interface RouteWithLayoutProps extends RouteProps {
    layout: Layout
    component: MakeRequired<RouteProps, 'component'>['component']
}

export const RouteWithLayout: FunctionComponent<RouteWithLayoutProps> = (props) => {
  const { layout: LayoutComponent, component: Component, ...rest } = props;


  const render = (routeComponentProps: RouteComponentProps): ReactElement => {
    return (
        <LayoutComponent>
            <Component {...routeComponentProps} />
        </LayoutComponent>
    )

  }
    return <Route {...rest} render={render} />
};

