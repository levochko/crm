import React, { FunctionComponent, useContext } from 'react';
import { Route, RouteProps, Redirect } from 'react-router-dom';
import { CircularProgress } from '@material-ui/core';
import { Layout } from 'layouts';
import { RouteWithLayout } from './RouteWithLayout';
import { RoutePath } from '../RoutePath';
import { StoreContext } from 'stores/StoreContext';
import { observer } from 'mobx-react';

interface CustomRouteProps extends RouteProps {
  layout?: Layout;
  forAuthorized?: boolean;
}

export const CustomRoute: FunctionComponent<CustomRouteProps> = observer(props => {
  const { layout: Layout, component: Component, forAuthorized, ...rest } = props;

  const store = useContext(StoreContext);
  const isAuthorized = store.isAuthorized.authorized;

  if (!Component) return <Redirect to={RoutePath.UNKNOWN} />;

  const getRoute = (RoutedComponent: FunctionComponent, RoutedPreloader: FunctionComponent) => {
    if (forAuthorized === undefined) return <RoutedComponent />;

    if (!forAuthorized && isAuthorized === true) return <Redirect to={RoutePath.BASE} />;
    if (!forAuthorized && !isAuthorized) return <RoutedComponent />;

    if (forAuthorized && isAuthorized === undefined) return <RoutedPreloader />;
    if (forAuthorized && isAuthorized === false) return <Redirect to={RoutePath.SIGN_IN} />;
    if (forAuthorized && isAuthorized === true) return <RoutedComponent />;

    return <Redirect to={RoutePath.UNKNOWN} />;
  };

  if (Layout) {
    const RoutedComponent = () => (
      <RouteWithLayout layout={Layout} component={Component} {...rest} />
    );
    const RoutedPreloader = () => (
      <RouteWithLayout layout={Layout} component={() => <CircularProgress />} {...rest} />
    );

    return getRoute(RoutedComponent, RoutedPreloader);
  } else {
    const RoutedComponent = () => <Route component={Component} {...rest} />;
    const RoutedPreloader = () => <Route component={() => <CircularProgress />} {...rest} />;

    return getRoute(RoutedComponent, RoutedPreloader);
  }
});
